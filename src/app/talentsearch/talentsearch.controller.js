(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('TalentsearchController', TalentsearchController);

  /** @ngInject */
  function TalentsearchController($scope, $window, $cookies, Restangular, $log) {
    var vm = $scope;

    vm._ = _;

    function talentSearchMenu() {
      vm.talentSearchMenu = $window.innerHeight + "px";
    }
    talentSearchMenu();

    vm.showTalentSearchResult = false;
    vm.searchTalentResult = [];

    vm.limit = 3;
    vm.standards = ["1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th", "11th", "12th"];
    vm.school_class = [{
        "title": "1st"
      },
      {
        "title": "2nd"
      },
      {
        "title": "3rd"
      },
      {
        "title": "4th"
      },
      {
        "title": "5th"
      },
      {
        "title": "6th"
      },
      {
        "title": "7th"
      },
      {
        "title": "8th"
      },
      {
        "title": "9th"
      },
      {
        "title": "10th"
      },
      {
        "title": "11th"
      },
      {
        "title": "12th"
      }
    ];
    vm.results = [{
        "title": "#footballers",
        "domainResult": [{
            "name": "Siddhartha Garg",
            "class": "10-A"
          },
          {
            "name": "Siddhartha Garg",
            "class": "11-B"
          },
          {
            "name": "Siddhartha Garg",
            "class": "9-D"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          }
        ],
        "expandResults": false,
        "defaultLimit": 3
      },
      {
        "title": "#debaters",
        "domainResult": [{
            "name": "Siddhartha Garg",
            "class": "10-A"
          },
          {
            "name": "Siddhartha Garg",
            "class": "11-B"
          },
          {
            "name": "Siddhartha Garg",
            "class": "9-D"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          },
          {
            "name": "Siddhartha Garg",
            "class": "12-C"
          }
        ],
        "expandResults": false,
        "defaultLimit": 3
      }

    ];



    vm.expand_more = function(index) {
      vm.results[index].defaultLimit = vm.results[index].domainResult.length;
      vm.results[index].expandResults = true;
    }

    vm.expand_more_search_results = function() {
      vm.searchTalentResult[0].defaultLimit = vm.searchTalentResult[0].domainResult.length;
      vm.searchTalentResult[0].expandResults = true;
    }
    vm.searchTalent = function(val) {
      var q = {
        query: val,
        type: $cookies.get('type'),
        access_token: $cookies.get('access_token')
      };

      Restangular
        .one('skills', 'talentSearch')
        .customPOST(q, null, null, null)
      .then(function(d) {
        $log.debug("POST Talent Search:", d);
        vm.showTalentSearchResult = true;

        if(d.response[0].student && d.response[0].skillName){
          vm.searchTalentResult[0] = {
            title: d.response[0].skillName,
            "expandResults": false,
            "defaultLimit": 3,
            domainResult: d.response[0].student
          };
          console.log(d.response[0].student);
        }

      })
      .catch(function (err) {
        $log.error("Cannot POST Talent Search:", err);
        vm.showTalentSearchResult = false;
      });

    }


  }
})();
