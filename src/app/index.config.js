(function() {
  'use strict';

  angular
    .module('frontend')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $locationProvider, RestangularProvider) {
    // Remove # break from URL
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    }).hashPrefix('!');


    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;



    // Restangular Config
    RestangularProvider.setBaseUrl("http://0.0.0.0:51657/api");
  }

})();
