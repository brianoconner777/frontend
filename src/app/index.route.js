(function() {
  'use strict';

  angular
    .module('frontend')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
    // Landing State
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        data: {
          css: [
            'app/main/main.css',
          ]
        }
      })

      .state('myspace',{
        url:'/myspace',
        templateUrl:'app/myspace/myspace.html',
      })

      .state('talentsearch',{
        url:'/talentsearch',
        templateUrl:'app/talentsearch/talentsearch.html',
        controller:'TalentsearchController',
        controllerAs:'talentsearch'
      })

      // Teacher States
      .state('teacherLogin', {
        url: '/teacher',
        templateUrl: 'app/teacher/login/login.html',
        controller: 'TeacherLoginController',
        controllerAs:'login'
      })

      .state('teacherEmailconf',{
        url:'/teacher/teacherEmailconf',
        templateUrl:'app/teacher/emailconf/emailconf.html',
        controller:'teacherEmailconfController',
        controllerAs:'emailconf'
      })

      .state('teacherResetPassword', {
        url: '/teacher/resetPassword',
        templateUrl: 'app/teacher/resetPassword/resetPassword.html',
        controller: 'TeacherResetPasswordController',
        controllerAs:'teacherResetPassword'
      })

      .state('teacherForgotPassword', {
        url: '/teacher/forgot',
        templateUrl: 'app/teacher/forgot/forgot.html',
        controller: 'TeacherForgotController',
        controllerAs:'teacherForgot'
      })

      .state('teacherProfile', {
        url: '/teacher/:id',
        templateUrl: 'app/teacher/profile/profile.html',
        controller: "teacherProfileController",
        controllerAs: "teacherProfile"
      })

      .state('teacherInitForm', {
        url: '/teacher/:id/initForm',
        views: {
          '': {
            templateUrl: 'app/teacher/initForm/initForm.html',
            controller: 'teacherInitFormController',
            controllerAs: 'initForm'
          },
          'stage1@teacherInitForm': {
            templateUrl: "app/teacher/initForm/views/stage1.html"
          },
          'stage2@teacherInitForm': {
            templateUrl: "app/teacher/initForm/views/stage2.html"
          },
          'stage3@teacherInitForm': {
            templateUrl: "app/teacher/initForm/views/stage3.html"
          },
          'stage4@teacherInitForm': {
            templateUrl: "app/teacher/initForm/views/stage4.html"
          }
        }
      })



      // Student States
      .state('studentLogin', {
        url: '/student',
        templateUrl: 'app/student/login/login.html',
        controller: 'StudentLoginController',
        controllerAs:'login'
      })

      .state('studentEmailconf',{
        url:'/student/studentEmailconf',
        templateUrl:'app/student/emailconf/emailconf.html',
        controller:'studentEmailconfController',
        controllerAs:'emailconf'
      })

      .state('studentResetPassword', {
        url: '/student/resetPassword',
        templateUrl: 'app/student/resetPassword/resetPassword.html',
        controller: 'StudentResetPasswordController',
        controllerAs:'studentResetPassword'
      })

      .state('studentForgotPassword', {
        url: '/student/forgot',
        templateUrl: 'app/student/forgot/forgot.html',
        controller: 'StudentForgotController',
        controllerAs:'studentForgot'
      })

      .state('studentProfile', {
        url: '/student/:id',
        templateUrl: 'app/student/profile/profile.html',
        controller: 'StudentProfileController',
        controllerAs:'profile'
      })

      .state('studentInitForm', {
        url: '/student/:id/initForm',
        views: {
          '': {
            templateUrl: 'app/student/initForm/initForm.html',
            controller: 'studentInitFormController',
            controllerAs: 'initForm'
          },
          'stage1@studentInitForm': {
            templateUrl: "app/student/initForm/views/stage1.html"
          },
          'stage2@studentInitForm': {
            templateUrl: "app/student/initForm/views/stage2.html"
          },
          'stage3@studentInitForm': {
            templateUrl: "app/student/initForm/views/stage3.html"
          },
          'stage4@studentInitForm': {
            templateUrl: "app/student/initForm/views/stage4.html"
          }
        }
      })


      // School States
      .state('schoolHome', {
        url: '/school/:id',
        templateUrl: 'app/school/home/home.html',
        controller:'HomeController',
        controllerAs:'home'
      })

      .state('schoolInitiative', {
        url: '/school/:id/initiative',
        templateUrl: 'app/school/initiative/initiative.html',
        controller:'InitiativesController',
        controllerAs:'initiatives'
      })

      .state('schoolGallery', {
        url: '/school/:id/gallery',
        templateUrl: 'app/school/gallery/gallery.html',
        controller:'AwardsController',
        controllerAs:'awards'
      })

      .state('schoolTeacher', {
        url: '/school/:id/teachers',
        templateUrl: 'app/school/teachers/teachers.html'
      })

      // Representatives
      .state('newsRepresentative', {
        url: '/:id/news',
        controller: 'newsRepController',
        controllerAs: 'newsRep',
        templateUrl: 'app/representative/news/newsRep.html'
      })
      .state('awardsRepresentative', {
        url: '/:id/awards',
        controller: 'awardsRepController',
        controllerAs: 'awardsRep',
        templateUrl: 'app/representative/awards/awardsRep.html'
      })
      .state('competitionsRepresentative', {
        url: '/:id/competitions',
        controller: 'competitionRepController',
        controllerAs: 'competitionRep',
        templateUrl: 'app/representative/competitions/competitionsRep.html'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
