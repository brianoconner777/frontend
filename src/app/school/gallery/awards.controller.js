'use strict';

angular
  .module('frontend')
  .controller('AwardsController', AwardsController);

function AwardsController($scope, $state, $log, Awards) {
	var vm = $scope;
  vm._ = _;
  vm.loading = {
    awards: true
  }
	vm.awardsContentLeftDefault = "100%";
	vm.awardsContentRightDefault = "00%";

  Awards.getAwards($state.params.id)
    .then(function(d) {
      $log.debug('GET Awards:', d);
      vm.awards_ = d;
      vm.loading.awards = false;
    })
    .catch(function(err){
      $log.error('Cannot GET Awards:', err);
      notie.alert(3, err, 2);
    });


	function awardOverlay(){
		vm.awardOlverlayHeight = window.innerHeight - 64 + "px";
	}	awardOverlay();

	vm.openAwardOverlay = function (award){

		vm.awardsContentLeftDefault = "70%";
		vm.awardsContentRightDefault = "30%";

		vm.awardOverlayTitle = award.title;
		vm.awardOverlayImg = award.img;
		vm.awardOverlayText = award.text;
	}

	vm.closeAwardOverlay = function() {
		vm.awardsContentLeftDefault = "100%";
		vm.awardsContentRightDefault = "00%";
	}


}


// vm.awards_ = [
//   {
//     title:"This is a sample Award",
//     img:"1.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"3.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"5.png",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },{
//     title:"This is a sample Award",
//     img:"7.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//
//   {
//     title:"This is a sample Award",
//     img:"9.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"11.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"2.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"4.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"6.png",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"8.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"8.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   },
//   {
//     title:"This is a sample Award",
//     img:"3.jpg",
//     text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
//   }
// ];
