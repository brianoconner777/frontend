'use strict';

angular
  .module('frontend')
  .controller('InitiativesController', InitiativesController);

function InitiativesController($scope) {
	var vm = $scope;
    vm._ = _;

	console.log("Working");
	vm.awardsContentLeftDefault = "100%";
	vm.awardsContentRightDefault = "00%";

	vm.awards_ = [
		{
			title:"This is a sample Award",
			img:"1.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"3.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"5.png",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},{
			title:"This is a sample Award",
			img:"7.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},

		{
			title:"This is a sample Award",
			img:"9.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"11.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"2.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"4.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"6.png",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"8.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"8.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		},
		{
			title:"This is a sample Award",
			img:"3.jpg",
			text:"First of all, just getting to the theater presents difficulties. Leaving a home equipped with a TV and a video recorder isn't an attractive idea on a humid, cold, or rainy night."
		}
	];

	function awardOverlay(){
		vm.awardOlverlayHeight = window.innerHeight - 64 + "px";
	}	awardOverlay();
	
	vm.openAwardOverlay = function (award){

		vm.awardsContentLeftDefault = "70%";
		vm.awardsContentRightDefault = "30%";

		vm.awardOverlayTitle = award.title;
		vm.awardOverlayImg = award.img;
		vm.awardOverlayText = award.text;
	}

	vm.closeAwardOverlay = function() {
		vm.awardsContentLeftDefault = "100%";
		vm.awardsContentRightDefault = "00%";
	}

}
