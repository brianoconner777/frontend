(function() {
  'use strict';

  angular
    .module('frontend')
    .service('School', School);

  function School(Restangular){
    return {

      /**
       * GET Endpoints
       */
      getSchool: function(schoolId) {
        return Restangular
          .one('schools', schoolId)
          .get();
      },

      getStudents: function(schoolId) {
        return Restangular
          .one('schools', schoolId)
          .get();
      }

    };
  }

})();
