'use strict';

angular
  .module('frontend')
  .controller('HomeController', HomeController);

function HomeController($scope, $state, $log, $timeout, News, Competitions, School) {
	var vm = $scope;
  vm._ = _;

  vm.loading = {
    news: true,
    competitions: true,
    school: true
  };

	vm.selectedTileIndex;
	vm.hideCompetiionPanel = true;

  /**
   * Initial Methods
   */
  School.getSchool($state.params.id)
    .then(function(d) {
      $log.debug('GET School:', d);
      vm.school = d;
      vm.loading.school = false;
    })
    .catch(function(err){
      $log.error('Cannot GET School:', err);
      notie.alert(3, err, 2);
    });


  News.getNews($state.params.id)
    .then(function(d) {
      $log.debug('GET News:', d);
      vm.news = d;
      vm.loading.news = false;
      $timeout(function(){
        $('.tooltipped').tooltip({delay: 50});
      })
    })
    .catch(function(err){
      $log.error('Cannot GET News:', d);
      notie.alert(3, err, 2);
    });


  Competitions.getCompetitions($state.params.id)
    .then(function(d) {
      $log.debug('GET competitions:', d);
      // vm.competitions = d;
      vm.loading.competitions = false;
      $timeout(function(){
        $('.tooltipped').tooltip({delay: 50});
      })
    })
    .catch(function(err){
      $log.error('Cannot GET competitions:', d);
      notie.alert(3, err, 2);
    });


  /**
   * Context Methods
   * @param  {[type]} competition [description]
   * @param  {[type]} index       [description]
   * @return {[type]}             [description]
   */
	vm.selectCompetition = function(competition, index){
		vm.selectedTileIndex = index;
		setSelectedTile(index);
		vm.hideCompetiionPanel = false;
		vm.competitionImg = competition.img;
		vm.competitionTitle = competition.title;
		vm.competitionDesc = competition.description;
	}
	vm.closeCompetition = function(){
		vm.hideCompetiionPanel = true;
		vm.competitions[vm.selectedTileIndex].selectedCss = "";
	}

  function setSelectedTile(index) {
    for(var i=0; i < vm.competitions.length; i++) {
      if(i == index) {
        vm.competitions[i].selectedCss = "z-depth-3";
      } else {
        vm.competitions[i].selectedCss = "";
      }
    }
  }

}
