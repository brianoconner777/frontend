'use strict';

angular
  .module('frontend')
  .controller('teacherEmailconfController', teacherEmailconfController);

function teacherEmailconfController($scope, Restangular, $log, $cookies) {
  var vm = $scope;
  /**
   * Scope Variables
   */
  vm.isvisible=false;

  /**
   * Scope Methods
   */
  vm.submit = function(){
    Restangular
      .one('teachers', $cookies.get('userId'))
      .customPOST({'email': vm.emailid}, 'updateEmail', null, {Authorization: $cookies.get('access_token')})
      .then(function(resp){
        $log.debug('Email Updated Verif. Sent:', resp);
        vm.isvisible = !vm.isvisible;

      })
      .catch(function(err) {
        $log.error('Email Update Verif Failed:', err);
      });
  }
}
