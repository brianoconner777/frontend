'use strict';

angular
  .module('frontend')
  .controller('TeacherForgotController', StudentForgotController);

function StudentForgotController($scope, $log, Teacher) {
  var vm = $scope;

  vm.email = undefined;

  vm.submit = function() {
    Teacher.resetPassword(vm.email)
      .then(function(d){
        $log.debug("POST Reset Password:", d);
        notie.alert(1, "Check your email!", 3);
      })
      .catch(function(err) {
        $log.debug("Cannot POST Reset Password:", err);
        notie.alert(3, "Failed to email!", 3);
      });
  }
}
