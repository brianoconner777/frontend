(function() {
  'use strict';

  angular
    .module('frontend')
    .service('Teacher', Teacher);

  function Teacher(Restangular, $log){
    $log.debug('Teacher Service Module');
    return {

      /**
       * GET Endpoints
       */
      getTeacherById: function(id, at, filters) {
        return Restangular
          .one('teachers')
          .customGET(id, filters, {'Authorization': at});
      },

      getSchool: function(id, at) {
        return Restangular
          .one('teachers', id)
          .customGET('school', null, {'Authorization': at});
      },

      getSkills: function(id, at) {
        return Restangular
          .one('teachers', id)
          .customGET('skill', null, {'Authorization': at});
      },

      getAwards: function(tId) {
        return Restangular
          .one('teachers', tId)
          .customGET('awards', null, null);
      },

      getProjects: function(tId) {
        return Restangular
          .one('teachers', tId)
          .customGET('projects', null, null);
      },

      getReviews: function(tId) {
        return Restangular
          .one('teachers', tId)
          .customGET('reviews', null, null);
      },

      exists: function(id, at) {
        return Restangular
          .one('teachers', id)
          .customGET('exists', null, {'Authorization': at});
      },

      endorse: function(sid, id, at) {
        return Restangular
          .one('teachers', id)
          .one('endorse', sid)
          .customGET(null, null, {'Authorization': at});
      },

      /**
       * PUT Endpoints
       */
      update: function(data, id, at) {
        return Restangular
          .one('teachers')
          .customPUT(data, id, null, {'Authorization': at});
      },

      /**
       * POST Endpoints
       */
      resetPassword: function(email) {
        var data = {
          'email': email,
        };
        return Restangular
          .one('teachers')
          .customPOST(data, 'reset', null, null);
      },

      resetPasswordConf: function(password, at) {
        var data = {
          'access_token': at,
          'password': password
        }
        return Restangular
          .one('teachers')
          .customPOST(data, 'resetPasswordConfirm', null, {'Authorization': at});
      },

      createSkill: function(id, sname, at) {
        var data = {skillName: sname};
        return Restangular
          .one('teachers', id)
          .customPOST(data, 'skill', null, {'Authorization': at});
      },

      createAward: function(tId, data, at) {
        data.lastUpdated = Date.now();
        return Restangular
          .one('teachers', tId)
          .customPOST(data, 'awards', null, {'Authorization': at});
      },

      createProject: function(tId, data, at) {
        data.lastUpdated = Date.now();
        return Restangular
          .one('teachers', tId)
          .customPOST(data, 'projects', null, {'Authorization': at});
      },

      createReview: function(tId, data, at) {
        data.time = Date.now();
        return Restangular
          .one('teachers', tId)
          .customPOST(data, 'reviews', null, {'Authorization': at});
      }
    }

  }

})();
