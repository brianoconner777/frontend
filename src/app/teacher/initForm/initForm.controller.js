'use strict';

angular
  .module('frontend')
  .controller('teacherInitFormController', teacherInitFormController);

function teacherInitFormController($scope, $timeout, $cookies, $state, $stateParams, $log, Restangular, Teacher, Upload, Student) {
  var vm = $scope;

  /**
   * Scope Initial Values
   */
  vm._ = _;
  vm.context = {
    progress: 1, // NOTE: This should be 1 as default
    safe: true, // NOTE: This should be false as default
    errMsg: "Enter Your New Password",
    exists: false
  };
  vm.form = undefined;
  vm.skills = [];
  vm.loader = {
    profile: true,
    file: false
  };

  vm.interests = [{
    name: "Algorithms"
  }, {
    name: "Analytics"
  }, {
    name: "Android"
  }, {
    name: "Applications"
  }, {
    name: "Blogging"
  }, {
    name: "Business"
  }, {
    name: "Business Analysis"
  }, {
    name: "Business Intelligence"
  }, {
    name: "Business Storytelling"
  }, {
    name: "Coaching"
  }, {
    name: "Cloud Computing"
  }, {
    name: "Communication"
  }, {
    name: "Computer"
  }, {
    name: "Consulting"
  }, {
    name: "Content"
  }, {
    name: "Content Management"
  }, {
    name: "Content Marketing"
  }, {
    name: "Content Strategy"
  }, {
    name: "Data Analysis"
  }, {
    name: "Data Analytics"
  }, {
    name: "Data Engineering"
  }, {
    name: "Data Mining"
  }, {
    name: "Data Science"
  }, {
    name: "Data Warehousing"
  }, {
    name: "Database Administration"
  }, {
    name: "Database Management"
  }, {
    name: "Digital Marketing"
  }];
  vm.languages = ["CHINESE", "MANDARIN", "SPANISH", "ENGLISH", "HINDI", "ARABIC", "PORTUGUESE", "BENGALI", "RUSSIAN"];



  /**
   * Controller's Init Procedures
   */
  Teacher.getTeacherById($cookies.get('userId'), $cookies.get('access_token'))
    .then(function(data) {
      $log.debug('GET Teacher By ID:', data);
      vm.form = data;
      if (!data.languages) vm.form.languages = [];

      for (var i = 0; i < vm.form.languages.length; i++)
        for (var j = 0; j < vm.languages.length; j++)
          if (vm.form.languages[i] == vm.languages[j])
            vm.languages.splice(j, 1);


      vm.form.initForm = 2;

      vm.loader.profile = false;
      if (!data.emailVerified) $state.go('TeacherLogin');
    })
    .catch(function(err) {
      $log.error('Cannot GET Teacher By ID:', err);
      vm.loader.profile = false;
      notie.alert(3, err.message, 2);
    });

  Teacher.getSchool($cookies.get('userId'), $cookies.get('access_token'))
    .then(function(data) {
      $log.debug('GET Teacher\'s school:', data);
      vm.context.school = data;
    })
    .catch(function(err) {
      $log.error('Cannot GET Teacher\'s school:', err);
      notie.alert(3, err.message, 2);
    });

  Teacher.getSkills($cookies.get('userId'), $cookies.get('access_token'))
    .then(function(data) {
      $log.debug('GET Teacher skils:', data);
      for (var i = 0; i < data.length; i++) {
        vm.skills.push(data[i].skillName);
        for (var j = 0; j < vm.interests.length; j++)
          if (data[i].skillName == vm.interests[j].name)
            vm.interests.splice(j, 1);
      }
    })
    .catch(function(err) {
      $log.error('Cannot GET Skills:', err);
      notie.alert(3, err.message, 2);
    });


  /**
   * Context Methods
   */
  vm.functions = {
    nextStage: function() {
      if (vm.context.safe) vm.context.progress++;
      else vm.functions.showError();
    },
    showError: function() {
      notie.alert(3, vm.context.errMsg, 2);
    },
    preprocess: function() {
      $timeout(function() {
        $('.dropdown-button').dropdown();
      });
    },

    // Stage 3
    search: function(str, ld) {
      var i, match, s, value,
        matches = [];

      for (i = 0; i < ld.length; i++) {
        match = false;

        value = ld[i].name
        match = findMatchString(value, str);
        if (match) {
          matches[matches.length] = ld[i];
        }
      }

      if(matches.length) return matches;
      else return [{name: str}];
    },

    interests: {
      add: function(skill) {
        var found = false;
        for (var i = 0; i < vm.skills.length; i++)
          if (vm.skills[i] == skill.title) found = true;
        if (!found)
          vm.skills.push(skill.title);
      },
      remove: function(skill, i) {
        vm.skills.splice(i, 1);
      }
    },


    languages: {
      add: function(skill, i) {
        vm.form.languages.push(skill);
        vm.languages.splice(i, 1);
      },
      remove: function(skill, i) {
        vm.form.languages.splice(i, 1);
        vm.languages.push(skill);
      }
    },

    // Stage 4
    verify: function() {
      Teacher
        .exists(vm.form.username, $cookies.get('access_token'))
        .then(function(data) {
          $log.debug('GET Teacher exist:', data);
          vm.context.exists = data.exists;
          if ($cookies.get('userId') == vm.form.username) vm.context.exists = false;
        })
        .catch(function(err) {
          $log.error('Failed GET Teacher existes:', err);
          notie.alert(3, err.data.message, 2);
        });

      Student
        .exists(vm.form.username, $cookies.get('access_token'))
        .then(function(data) {
          $log.debug('GET Teacher exist:', data);
          vm.context.exists = data.exists;
        })
        .catch(function(err) {
          $log.error('Failed GET Teacher existes:', err);
          notie.alert(3, err.data.message, 2);
        });
    },
    uploadFiles: function(file, invFile) {
      if (invFile.length) {
        $log.debug('Invalid File Size');
        notie.alert(3, 'Upload Files upto 5MB', 3);
      }


      if (file) {
        var url = window.location.protocol + "//" + window.location.hostname + ":51657/api/containers/dextr123/upload";

        vm.loader.file = true;
        Upload.upload({
            url: url,
            data: {
              file: file
            }
          })
          .then(function(resp) {
            $log.debug("POST S3 Profile Picture:", resp);

            vm.loader.file = false;

            try {
              vm.form.image = resp.data.result.files.file[0].providerResponse.location;
              $log.warn("Profile Picture:", vm.form.image);
            } catch (e) {
              $log.error("Cannot Parse Response")
            }
          }, function(err) {
            notie.alert(3, err, 2);
          }, function(evt) {
            vm.loader.file = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            $log.debug("Progress:", vm.loader.file)
          });
      }
    },
    submit: function() {
      if (vm.form.image == undefined) {
        notie.alert(3, "Upload Your Profile Picture", 2);
        return;
      }

      var process = 2 + vm.skills.length;

      // Update Profile
      Teacher
        .update(vm.form, $cookies.get('userId'), $cookies.get('access_token'))
        .then(function(resp) {
          $log.debug('PUT Teacher update:', resp);
          notie.alert(1, 'Updated Your Profile', 2);

          process--;
          if (!process) $state.go('teacherProfile', {
            id: $stateParams.id
          })
        })
        .catch(function(err) {
          $log.error('Cannot PUT Teacher update:', err);
          notie.alert(3, err.data.message, 2);
        });

      // Update Password
      Teacher
        .resetPassword(vm.form.email)
        .then(function(res) {
          $log.debug('POST Teacher reset:', res);

          process--;
          if (!process) $state.go('teacherProfile', {
            id: $stateParams.id
          })
        })
        .catch(function(err) {
          $log.error('Cannot POST Teacher reset:', err);
          notie.alert(3, err.data.message, 2);
        });

      // Create Skills
      for (var i = 0; i < vm.skills.length; i++)
        Teacher
        .createSkill($cookies.get('userId'), vm.skills[i], $cookies.get('access_token'))
        .then(function(res) {
          $log.debug('POST Create Skill:', res);

          process--;
          if (!process) $state.go('teacherProfile', {
            id: $stateParams.id
          })
        })
        .catch(function(err) {
          $log.error('Cannot POST Create Skill:', err);
        });
    }
  };

  function findMatchString(target, str) {
    var result, matches, re;
    re = new RegExp(str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'i');
    if (!target) { return; }
    if (!target.match || !target.replace) { target = target.toString(); }
    matches = target.match(re);
    if(matches)
      return true;
    else return false;
  }

}
