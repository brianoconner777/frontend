(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('TeacherLoginController',TeacherLoginController);

    TeacherLoginController.$inject = ['$scope', '$log', 'Restangular', '$cookies', '$state'];

  function TeacherLoginController($scope, $log, Restangular, $cookies, $state){
    var vm = $scope;

    /**
     * Scope Variables
     */
    vm.data = {
      username: undefined,
      password: undefined
    };
    vm.functions = {};

    /**
     * Scope Functions
     */
    vm.functions.auth = function(){
      Restangular
        .one('teachers')
        .post('login', {
          "username": vm.data.username,
          "password": vm.data.password
        })
        .then(function(response){
          $log.debug("Received teacher login: ", response);
          Materialize.toast("Successfully logged in", 2000);

          $cookies.put('access_token', response.id);
          $cookies.put('userId', response.userId);
          $cookies.put('type', "teacher");

          if(response.initForm == 0)
            $state.go('teacherEmailconf');
          else if(response.initForm == 1)
            $state.go('teacherInitForm', {id: response.userId});
          else $state.go('teacherProfile', { id: response.userId });
        })
        .catch(function(response){
          $log.warn("Failed teacher login: ", response);
          Materialize.toast("Failed to login", 2000);
        });
    };
  }

})();
