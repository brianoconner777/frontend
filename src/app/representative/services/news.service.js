'use strict';

angular
  .module('frontend')
  .factory('News', News);

function News(Restangular) {
  return {
    getNews: function(schoolId) {
      return Restangular
        .one('schools', schoolId)
        .one('news')
        .get();
    },

    getPrincipals: function() {
      return Restangular
      .one('representatives', 'newsRep')
      .one('principals')
      .get();
    },

    updateById: function(schoolId, newsId, data, at) {
      return Restangular
        .one('schools', schoolId)
        .one('news')
        .customPUT(data, newsId, null, {'Authorization': at});
    },

    deleteById: function(schoolId, newsId, at) {
      return Restangular
        .one('schools', schoolId)
        .one('news')
        .customDELETE(newsId, null, {'Authorization': at});
    },

    create: function(data, schoolId, at) {
      return Restangular
        .one('schools', schoolId)
        .customPOST(data, 'news', null, {'Authorization': at});
    },

  };
}
