'use strict';

angular
  .module('frontend')
  .factory('Awards', Awards);

function Awards(Restangular) {
  return {
    getAwards: function(schoolId) {
      return Restangular
        .one('schools', schoolId)
        .one('awards')
        .get();
    },

    getPrincipals: function() {
      return Restangular
      .one('representatives', 'awardsRep')
      .one('principals')
      .get();
    },

    updateById: function(schoolId, awardsId, data, at) {
      return Restangular
        .one('schools', schoolId)
        .one('awards')
        .customPUT(data, awardsId, null, {'Authorization': at});
    },

    deleteById: function(schoolId, awardsId, at) {
      return Restangular
        .one('schools', schoolId)
        .one('awards')
        .customDELETE(awardsId, null, {'Authorization': at});
    },

    create: function(data, schoolId, at) {
      return Restangular
        .one('schools', schoolId)
        .customPOST(data, 'awards', null, {'Authorization': at});
    },

  };
}
