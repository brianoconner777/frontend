'use strict';

angular
  .module('frontend')
  .factory('Competitions', Competitions);

function Competitions(Restangular) {
  return {
    getCompetitions: function(schoolId) {
      return Restangular
        .one('schools', schoolId)
        .one('competitions')
        .get();
    },

    getPrincipals: function() {
      return Restangular
      .one('representatives', 'compRep')
      .one('principals')
      .get();
    },

    updateById: function(schoolId, competitionsId, data, at) {
      return Restangular
        .one('schools', schoolId)
        .one('competitions')
        .customPUT(data, competitionsId, null, {'Authorization': at});
    },

    deleteById: function(schoolId, competitionsId, at) {
      return Restangular
        .one('schools', schoolId)
        .one('competitions')
        .customDELETE(competitionsId, null, {'Authorization': at});
    },

    create: function(data, schoolId, at) {
      return Restangular
        .one('schools', schoolId)
        .customPOST(data, 'competitions', null, {'Authorization': at});
    },

  };
}
