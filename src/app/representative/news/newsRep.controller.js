'use strict';

angular
  .module('frontend')
  .controller('newsRepController', newsRepController);

function newsRepController($scope, $timeout, $log, $cookies, $state, $sce, News, Upload) {
  var vm = $scope;

  vm._ = _;

  vm.context = {
    edit: {},
    add: {
      documents: []
    }
  };
  vm.principal = {};
  vm.news = [];
  vm.loading = {
    getNews: true,
    uploading: false,
    file: false,
    profile: true
  }

  /**
   * Initial Methods
   */
  News.getPrincipals()
    .then(function(d) {
      $log.debug('GET principals:', d);

      var found = false;
      for (var i = 0; i < d.length; i++)
        if(d[i].principalId == $cookies.get('userId')) {
          found = true;
          vm.principal = d[i];
          refresh();
        }

      if(!found) {
        notie.alert(3, "Not Authorized!!", 3);
        $state.go('home');
      }
    })
    .catch(function(err) {
      $log.error('Cannot GET principals:', err);
    });


  /**
   * Context Methods
   */
  vm.functions = {
    trust: function(url) {
      return $sce.trustAsResourceUrl(url);
    },

    edit: function(i) {
      vm.context.edit = vm.news[i]
    },

    update: function() {
      News.updateById(vm.principal.schoolId, vm.context.edit.id, vm.context.edit, $cookies.get('access_token'))
        .then(function(d) {
          $log.debug('PUT News:', d);
          notie.alert(1, "Updated Successfully", 3);
          refresh();
        })
        .catch(function(err) {
          $log.error('Cannot PUT News:', err);
        });

    },

    create: function() {
      vm.context.add.lastUpdated = Date.now();
      News.create(vm.context.add, vm.principal.schoolId, $cookies.get('access_token'))
        .then(function(d) {
          $log.debug('POST Create News:', d);

          notie.alert(1, "Create News", 3);
          refresh();
        })
        .catch(function(err) {
          $log.error('Cannot POST create News:', err);
          notie.alert(3, "Cannot Create Post", 3);
        });

    },

    delete: function(newsId) {
      News.deleteById(vm.principal.schoolId, newsId, $cookies.get('access_token'))
        .then(function(d) {
          $log.debug("DELETE News:", d);
          notie.alert(1, "Deleted news", 3);
        })
        .catch(function(err) {
          $log.error(3, err.data.message, 3);
        });
    },

    uploadFiles: function(file, invFile) {
      if(invFile.length) {
        $log.debug('Invalid File Size');
        notie.alert(3, 'Upload Files upto 5MB', 3);
      }


      if(file) {
        var url = window.location.protocol+"//"+window.location.hostname + ":51657/api/containers/dextr123/upload";

        vm.loading.file = true;
        Upload.upload({
          url: url,
          data: {file: file}
        })
        .then(function(resp) {
          $log.debug("POST S3 Document:", resp);

          vm.loading.file = false;

          try {
            var link = resp.data.result.files.file[0].providerResponse.location;
            var name = resp.data.result.files.file[0].providerResponse.name;

            vm.context.add.documents.push({
              link: link,
              name: name
            });
            $log.warn("Document Link:", link);
          } catch (e) {
            $log.error("Cannot Parse Response")
          }
        }, function(err) {
          notie.alert(3, err, 2);
        });
      }
    }
  };

  function refresh() {
    News.getNews(vm.principal.schoolId)
      .then(function(d) {
        $log.debug('GET News:', d);
        vm.loading.getNews = false;
        vm.news = d;

        $timeout(function() {
          $('#addNewsModal').modal();
          $('#editNewsModal').modal();
          $('.modal-trigger').modal();
        });
      })
      .catch(function(err) {
        $log.error('Cannot GET News:', err);
      });
  }

  $timeout(function() {
    $('#addNewsModal').modal();
    $('#editNewsModal').modal();
    $('.modal-trigger').modal();
  });
}
