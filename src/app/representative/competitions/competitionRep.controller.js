'use strict';

angular
  .module('frontend')
  .controller('competitionRepController', competitionRepController);

function competitionRepController($scope, $timeout, $log, $cookies, $state, $sce, Competitions, Upload) {
  var vm = $scope;

  vm._ = _;

  vm.context = {
    edit: {},
    add: {}
  };
  vm.principal = {};
  vm.competitions = [];
  vm.loading = {
    getCompetitions: true,
    uploading: false,
    file: false,
    profile: true
  }

  /**
   * Initial Methods
   */
  Competitions.getPrincipals()
    .then(function(d) {
      $log.debug('GET principals:', d);

      var found = false;
      for (var i = 0; i < d.length; i++)
        if(d[i].principalId == $cookies.get('userId')) {
          found = true;
          vm.principal = d[i];
          refresh();
        }

      if(!found) {
        notie.alert(3, "Not Authorized!!", 3);
        $state.go('home');
      }
    })
    .catch(function(err) {
      $log.error('Cannot GET principals:', err);
    });


    /**
     * Context Methods
     */
    vm.functions = {
      trust: function(url) {
        return $sce.trustAsResourceUrl(url);
      },

      delImage: function(mode) {
        vm.context[mode].image = null;
      },

      edit: function(i) {
        vm.context.edit = vm.competitions[i]
      },

      update: function() {
        Competitions.updateById(vm.principal.schoolId, vm.context.edit.id, vm.context.edit, $cookies.get('access_token'))
          .then(function(d) {
            $log.debug('PUT Competitions:', d);
            notie.alert(1, "Updated Successfully", 3);
            refresh();
          })
          .catch(function(err) {
            $log.error('Cannot PUT Competitions:', err);
          });

      },

      create: function() {
        vm.context.add.lastUpdated = Date.now();
        Competitions.create(vm.context.add, vm.principal.schoolId, $cookies.get('access_token'))
          .then(function(d) {
            $log.debug('POST Create Competitions:', d);

            notie.alert(1, "Create Competitions", 3);
            refresh();
          })
          .catch(function(err) {
            $log.error('Cannot POST create Competitions:', err);
            notie.alert(3, "Cannot Create Post", 3);
          });

      },

      delete: function(compId) {
        Competitions.deleteById(vm.principal.schoolId, compId, $cookies.get('access_token'))
          .then(function(d) {
            $log.debug("DELETE Competitions:", d);
            notie.alert(1, "Deleted competitions", 3);
          })
          .catch(function(err) {
            $log.error(3, err.data.message, 3);
          });
      },

      uploadFiles: function(mode, file, invFile) {
        if(invFile.length) {
          $log.debug('Invalid File Size');
          notie.alert(3, 'Upload Files upto 5MB', 3);
        }


        if(file) {
          var url = window.location.protocol+"//"+window.location.hostname + ":51657/api/containers/dextr123/upload";

          vm.loading.file = true;
          Upload.upload({
            url: url,
            data: {file: file}
          })
          .then(function(resp) {
            $log.debug("POST S3 Document:", resp);

            vm.loading.file = false;

            try {
              var link = resp.data.result.files.file[0].providerResponse.location;
              var name = resp.data.result.files.file[0].providerResponse.name;

              vm.context[mode].image = {
                link: link,
                name: name
              };
              $log.warn("Document Link:", link);
            } catch (e) {
              $log.error("Cannot Parse Response")
            }
          }, function(err) {
            notie.alert(3, err, 2);
          });
        }
      }
    };

    function refresh() {
      Competitions.getCompetitions(vm.principal.schoolId)
        .then(function(d) {
          $log.debug('GET Competitions:', d);
          vm.loading.getCompetitions = false;
          vm.competitions = d;

          $timeout(function() {
            $('#addCompetitionsModal').modal();
            $('#editCompetitionsModal').modal();
            $('.modal-trigger').modal();
          });
        })
        .catch(function(err) {
          $log.error('Cannot GET Competitions:', err);
        });
    }

    $timeout(function() {
      $('#addCompetitionsModal').modal();
      $('#editCompetitionsModal').modal();
      $('.modal-trigger').modal();
    });
}
