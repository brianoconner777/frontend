'use strict';

angular
  .module('frontend')
  .controller('StudentResetPasswordController', StudentResetPasswordController);

function StudentResetPasswordController($scope, $location, $log, $state, Student) {
  var vm = $scope;

  vm.submit = function() {
    var at = $location.search().access_token;

    if(at == undefined)
      notie.alert(3, "Access Token is Missing", 3);
    else {
      if(vm.password |
         vm.confPassword |
         vm.password !== vm.confPassword)
        notie.alert(3, "Password Does Not Match", 3);
      else
        Student.resetPasswordConf(vm.password, at)
          .then(function(d){
            $log.debug("POST Student Reset Password:", d);
            notie.alert(1, "Password changed Successfully", 3);
            $state.go('teacherLogin');
          })
          .catch(function(err){
            $log.error("Cannot POST Student Reset Password:", err);
            notie.alert(3, err.data.message, 3);
          });
    }
  };

}
