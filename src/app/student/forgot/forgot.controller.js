'use strict';

angular
  .module('frontend')
  .controller('StudentForgotController', StudentForgotController);

function StudentForgotController($scope, $log, Student) {
  var vm = $scope;

  vm.email = undefined;

  vm.submit = function() {
    Student.resetPassword(vm.email)
      .then(function(d){
        $log.debug("POST Reset Password:", d);
        notie.alert(1, "Check your email!", 3);
      })
      .catch(function(err) {
        $log.debug("Cannot POST Reset Password:", err);
        notie.alert(3, "Failed to email!", 3);
      });
  }
}
