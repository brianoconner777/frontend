'use strict';

angular
  .module('frontend')
  .controller('StudentProfileController', StudentProfileController);

function StudentProfileController($scope, $cookies, $log, $stateParams, Student, Teacher) {
  /**
   * Scope Value Declaration
   */
  var vm = $scope;

  vm._ = _;

  vm.defaultLeftWidth = "100%";
  vm.defaultRightWidth = "0%";

  vm.profileEditorMainWidth = "100%";
  vm.profileEditorMainDisplay = "";
  vm.profileEditorInnerWidth = "0%";
  vm.profileEditorInnerDisplay = "none";

  vm.leftContentContainer = "s3 displayblock";
  vm.middleContentContainer = "s6 displayblock";
  vm.rightContentContainer = "s3 displayblock";

  vm.toggleSwitch = 1;

  vm.loader = {
    profile: true
  };
  vm.form = undefined;
  vm.skills = undefined;
  vm.context = {
    reviewToggle: false
  };
  vm.awards = [];
  vm.projects = [];
  vm.reviews = [];

  vm.update = {
    review: {},
    awards: {},
    password: {},
    projects: {}
  };

  vm.interests = [ {name: "Algorithms"}, {name: "Analytics"}, {name: "Android"}, {name: "Applications"}, {name: "Blogging"}, {name: "Business"}, {name: "Business Analysis"}, {name: "Business Intelligence"}, {name: "Business Storytelling"}, {name: "Coaching"}, {name: "Cloud Computing"}, {name: "Communication"}, {name: "Computer"}, {name: "Consulting"}, {name: "Content"}, {name: "Content Management"}, {name: "Content Marketing"}, {name: "Content Strategy"}, {name: "Data Analysis"}, {name: "Data Analytics"}, {name: "Data Engineering"}, {name: "Data Mining"}, {name: "Data Science"}, {name: "Data Warehousing"}, {name: "Database Administration"}, {name: "Database Management"}, {name: "Digital Marketing"} ];

  function editProfileHeight() {
    vm.profileEditorHeight = window.innerHeight - 64;
  }
  editProfileHeight();


  vm.profileItems = [{
      "title": "Account",
      "icon": "account_circle"
    }, {
      "title": "Details",
      "icon": "list"
    }, {
      "title": "Interests",
      "icon": "favorite"
    }, {
      "title": "Projects",
      "icon": "import_contacts"
    }, {
      "title": "Awards",
      "icon": "folder_special"
    }, {
      "title": "Bio",
      "icon": "chrome_reader_mode"
    },
    // {"title":"Appearance","icon":"face"},
    {
      "title": "Password Management",
      "icon": "lock_outline"
    }

  ];
  vm.displayArr = ["none", "none", "none", "none", "none", "none", "none", "none"];

  vm.editorSequenceBack = function() {

    vm.profileEditorMainWidth = "100%";
    vm.profileEditorMainDisplay = "block";
    vm.profileEditorInnerWidth = "0%";
    vm.profileEditorInnerDisplay = "none";
  }

  vm.profileSequence = function(profileItem) {

    vm.selectedProfileItem = profileItem;
    vm.profileEditorMainWidth = "0%";
    vm.profileEditorMainDisplay = "none";
    vm.profileEditorInnerWidth = "100%";
    vm.profileEditorInnerDisplay = "block";

    if (profileItem == "Account") {
      vm.displayArr = ["block", "none", "none", "none", "none", "none", "none", "none"];
    } else if (profileItem == "Details") {
      vm.displayArr = ["none", "block", "none", "none", "none", "none", "none", "none"];
    } else if (profileItem == "Interests") {
      vm.displayArr = ["none", "none", "block", "none", "none", "none", "none", "none"];
    } else if (profileItem == "Projects") {
      vm.displayArr = ["none", "none", "none", "block", "none", "none", "none", "none"];
    } else if (profileItem == "Awards") {
      vm.displayArr = ["none", "none", "none", "none", "block", "none", "none", "none"];
    } else if (profileItem == "Bio") {
      vm.displayArr = ["none", "none", "none", "none", "none", "block", "none", "none"];
    }
    // else if(profileItem == "Appearance"){
    // 	vm.displayArr = ["none","none","none","none","none","none","block","none"];
    // }
    else if (profileItem == "Password Management") {
      vm.displayArr = ["none", "none", "none", "none", "none", "none", "none", "block"];
    }
  }


  /**
   * Controller's Init Network Calls
   */
  Student.getStudentById($stateParams.id, $cookies.get('access_token'))
    .then(function(data) {
      $log.debug('GET Student By ID:', data);
      vm.form = data;
      if (!data.interests) vm.form.interests = [];
      if (!data.languages) vm.form.languages = [];

      if ($cookies.get('userId') == $stateParams.id)
        $cookies.put("schoolId", data.schoolId)

      vm.loader.profile = false;
      if (!data.emailVerified) $state.go('studentLogin');
    })
    .catch(function(err) {
      $log.error('Cannot GET Student By ID:', err);
      vm.loader.profile = false;
      notie.alert(3, err.message, 2);
    });

  Student.getSchool($stateParams.id, $cookies.get('access_token'))
    .then(function(data) {
      $log.debug('GET Student\'s school:', data);
      vm.context.school = data;
    })
    .catch(function(err) {
      $log.error('Cannot GET Student\'s school:', err);
      notie.alert(3, err.message, 2);
    });

  Student.getSkills($stateParams.id, $cookies.get('access_token'))
    .then(function(d) {
      $log.debug('GET Skill Name:', d);
      vm.skills = d;
      for (var i = 0; i < d.length; i++)
        for (var j = 0; j < vm.interests.length; j++)
          if (d[i].skillName == vm.interests[j].name)
            vm.interests.splice(j, 1);

    })
    .catch(function(err) {
      $log.error('Cannot GET Skill Name:', err);
      notie.alert(3, err.message, 2);
    });

  Student.getAwards($stateParams.id)
    .then(function(d) {
      $log.debug('GET Awards Name:', d);
      vm.awards = d;
    })
    .catch(function(err) {
      $log.error('Cannot GET Awards Name:', err);
      notie.alert(3, err.message, 2);
    });


  Student.getProjects($stateParams.id)
    .then(function(d) {
      $log.debug('GET Projects Name:', d);
      vm.projects = d;
    })
    .catch(function(err) {
      $log.error('Cannot GET Projects Name:', err);
      notie.alert(3, err.message, 2);
    });

  Student.getReviews($stateParams.id)
    .then(function(d) {
      $log.debug('GET Reviews:', d);

      for (var i = 0; i < d.length; i++) {
        if (d[i].reviewerType == "student")
          Student.getStudentById(d[i].reviewerId, $cookies.get('access_token'))
          .then(function(r) {
            var index = _.findIndex(d, {
              'reviewerId': r.username
            });

            d[index].reviewer = r;
            vm.reviews.push(d[index]);
            d.splice(index, 1);

            $log.debug("GET Reviewer Details:", d);
          })
          .catch(function(err) {
            $log.error("Cannot GET Reviewer Details:", err);
          });
        else Teacher.getTeacherById(d[i].reviewerId, $cookies.get('access_token'))
          .then(function(r) {
            var index = _.findIndex(d, {
              'reviewerId': r.username
            });

            d[index].reviewer = r;
            vm.reviews.push(d[index]);
            d.splice(index, 1);

            $log.debug("GET Reviewer Details:", d);
          })
          .catch(function(err) {
            $log.error("Cannot GET Reviewer Details:", err);
          });
      }
    })
    .catch(function(err) {
      $log.error('Cannot GET Reviews Name:', err);
      notie.alert(3, err.message, 2);
    });


  /**
   * Scope Functions
   */
  vm.functions = {
    search: function(str, ld) {
      var i, match, s, value,
        matches = [];

      for (i = 0; i < ld.length; i++) {
        match = false;

        value = ld[i].name
        match = findMatchString(value, str);
        if (match) {
          matches[matches.length] = ld[i];
        }
      }

      if(matches.length) return matches;
      else return [{name: str}];
    },

    endorse: function(sid) {
      Student.endorse(sid, $cookies.get('userId'), $cookies.get('access_token'))
        .then(function(resp) {
          $log.debug('GET Student Endorse:', resp);

          Student.getSkills($stateParams.id, $cookies.get('access_token'))
            .then(function(d) {
              $log.debug('GET Skill Name:', d);
              vm.skills = d;
            })
            .catch(function(err) {
              $log.error('Cannot GET Skill Name:', err);
              notie.alert(3, err.message, 2);
            });

        })
        .catch(function(err) {
          $log.error('Cannot GET Student Endorse:', err);

          notie.alert(3, err.data.error.message, 2);
        });
    },

    createAward: function() {
      Student.createAward($stateParams.id, vm.update.awards, $cookies.get('access_token'))
        .then(function(d) {
          $log.debug('POST Student Create Awards:', d);
          notie.alert(1, "Awards Created", 2);

          Student.getAwards($stateParams.id)
            .then(function(d) {
              $log.debug('GET Awards Name:', d);
              vm.awards = d;
            })
            .catch(function(err) {
              $log.error('Cannot GET Awards Name:', err);
              notie.alert(3, err.message, 2);
            });
        })
        .catch(function(err) {
          $log.error('Cannot POST Student Create Awards:', err);
          notie.alert(3, err.data.error.message, 2);
        });
    },

    createProject: function() {
      Student.createProject($stateParams.id, vm.update.projects, $cookies.get('access_token'))
        .then(function(d) {
          $log.debug('POST Student Create Projects:', d);
          notie.alert(1, "Project Created Successfully!", 2);

          Student.getProjects($stateParams.id)
            .then(function(d) {
              $log.debug('GET Projects Name:', d);
              vm.projects = d;
            })
            .catch(function(err) {
              $log.error('Cannot GET Projects Name:', err);
              notie.alert(3, err.message, 2);
            });
        })
        .catch(function(err) {
          $log.error('Cannot POST Student Create Projects:', err);
          notie.alert(3, err.data.error.message, 2);
        });
    },

    updatepassword: function() {
      if (vm.update.password.newPassword == vm.update.password.confPassword) {
        Student.resetPassword(vm.update.password.email)
          .then(function(d) {
            $log.debug('POST student reset:', d);
            notie.alert(1, "Password Reset Mail Sent!", 3);
          })
          .catch(function(err) {
            $log.error('Cannot POST student reset:', err);
            notie.alert(3, err.data.message, 2);
          });
      } else notie.laert(3, "Pasword does not match", 3);
    },

    createSkill: function(skill) {
      Student
        .createSkill($cookies.get('userId'), skill.title, $cookies.get('access_token'))
        .then(function(res) {
          $log.debug('POST Create Skill:', res);
          notie.alert(1, "Skill Added", 3);

          Student.getSkills($stateParams.id, $cookies.get('access_token'))
            .then(function(d) {
              $log.debug('GET Skill Name:', d);
              vm.skills = d;
              for (var i = 0; i < d.length; i++)
                for (var j = 0; j < vm.interests.length; j++)
                  if (d[i].skillName == vm.interests[j].name)
                    vm.interests.splice(j, 1);
            })
            .catch(function(err) {
              $log.error('Cannot GET Skill Name:', err);
              notie.alert(3, err.message, 2);
            });
        })
        .catch(function(err) {
          $log.error('Cannot POST Create Skill:', err);
        });
    },

    updateProfile: function() {
      Student
        .update(vm.form, $cookies.get('userId'), $cookies.get('access_token'))
        .then(function(resp) {
          $log.debug('PUT student update:', resp);
          notie.alert(1, 'Updated Your Profile', 2);
        })
        .catch(function(err) {
          $log.error('Cannot PUT student update:', err);
          notie.alert(3, err.data.message, 2);
        });
    },

    openReviewerProfile: function(i) {
      if (vm.reviews[i].reviewerType == "student")
        $state.go("studentProfile", {
          id: vm.review[i].reviewerId
        })
      else if (vm.reviews[i].reviewerType == "teacher")
        $state.go("teacherProfile", {
          id: vm.review[i].reviewerId
        })
    },

    addReview: function() {
      Student.createReview($stateParams.id, vm.update.review, $cookies.get('access_token'))
        .then(function(resp) {
          $log.debug('POST Create Review:', resp);
          notie.alert(1, 'Review Added!', 2);
        })
        .catch(function(err) {
          $log.error('Cannot POST Create review:', err);
          notie.alert(3, err.data.error.message, 2);
        });

    },

    isOwner: function() {
      if ($cookies.get('userId') == $stateParams.id) return true;
      else return false;
    }
  };

  function findMatchString(target, str) {
    var result, matches, re;
    re = new RegExp(str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'i');
    if (!target) { return; }
    if (!target.match || !target.replace) { target = target.toString(); }
    matches = target.match(re);
    if(matches)
      return true;
    else return false;
  }

  /**
   * Scope Listeners
   */
  vm.$on('updateProfileToggle', function(event, args) {
    vm.toggleSwitch = !vm.toggleSwitch;
    if (vm.toggleSwitch) {
      vm.defaultLeftWidth = "100%";
      vm.defaultRightWidth = "0%";

      vm.leftContentContainer = "s3 displayblock";
      vm.middleContentContainer = "s6 displayblock";
      vm.rightContentContainer = "s3 displayblock";
    } else {
      vm.defaultLeftWidth = "75%";
      vm.defaultRightWidth = "25%";

      vm.leftContentContainer = "s0 displaynone";
      vm.middleContentContainer = "s12 displayblock";
      vm.rightContentContainer = "s0 displaynone";
    }
  });
}
