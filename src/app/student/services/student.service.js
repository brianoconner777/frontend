(function() {
  'use strict';

  angular
    .module('frontend')
    .service('Student', Student);

  function Student(Restangular, $log){
    $log.debug('Student Service Module');
    return {

      /**
       * GET Endpoints
       */
       find: function(filters, at) {
         return Restangular
          .one('students')
          .customGET(null, filters, {'Authorization': at});
       },

      getStudentById: function(id, at, filters) {
        return Restangular
          .one('students')
          .customGET(id, filters, {'Authorization': at});
      },

      getSchool: function(id, at) {
        return Restangular
          .one('students', id)
          .customGET('school', null, {'Authorization': at});
      },

      getSkills: function(id, at) {
        return Restangular
          .one('students', id)
          .customGET('skill', null, {'Authorization': at});
      },

      getAwards: function(sId) {
        return Restangular
          .one('students', sId)
          .customGET('awards', null, null);
      },

      getProjects: function(sId) {
        return Restangular
          .one('students', sId)
          .customGET('projects', null, null);
      },

      getReviews: function(sId) {
        return Restangular
          .one('students', sId)
          .customGET('reviews', null, null);
      },

      exists: function(id, at) {
        return Restangular
          .one('students', id)
          .customGET('exists', null, {'Authorization': at});
      },

      endorse: function(sid, id, at) {
        return Restangular
          .one('students', id)
          .one('endorse', sid)
          .customGET(null, null, {'Authorization': at});
      },

      /**
       * PUT Endpoints
       */
      update: function(data, id, at) {
        return Restangular
          .one('students')
          .customPUT(data, id, null, {'Authorization': at});
      },

      /**
       * POST Endpoints
       */
      resetPassword: function(email) {
        var data = {
          'email': email
        };
        return Restangular
          .one('students')
          .customPOST(data, 'reset', null, null);
      },

      resetPasswordConf: function(password, at) {
        var data = {
          'access_token': at,
          'password': password
        }
        return Restangular
          .one('students')
          .customPOST(data, 'resetPasswordConfirm', null, {'Authorization': at});
      },

      createSkill: function(id, sname, at) {
        var data = {skillName: sname};
        return Restangular
          .one('students', id)
          .customPOST(data, 'skill', null, {'Authorization': at});
      },

      createAward: function(sId, data, at) {
        data.lastUpdated = Date.now();
        return Restangular
          .one('students', sId)
          .customPOST(data, 'awards', null, {'Authorization': at});
      },

      createProject: function(sId, data, at) {
        data.lastUpdated = Date.now();
        return Restangular
          .one('students', sId)
          .customPOST(data, 'projects', null, {'Authorization': at});
      },

      createReview: function(sId, data, at) {
        data.time = Date.now();
        return Restangular
          .one('students', sId)
          .customPOST(data, 'reviews', null, {'Authorization': at});
      }
    }

  }

})();
