(function() {
  'use strict';

  angular
    .module('frontend')
    .controller('StudentLoginController',StudentLoginController);

    StudentLoginController.$inject = ['$scope', '$log', 'Restangular', '$cookies', '$state'];

  function StudentLoginController($scope, $log, Restangular, $cookies, $state){
    var vm = $scope;

    /**
     * Scope Variables
     */
    vm.data = {
      username: undefined,
      password: undefined
    };
    vm.functions = {};

    /**
     * Scope Functions
     */
    vm.functions.auth = function(){
      Restangular
        .one('students')
        .post('login', {
          "username": vm.data.username,
          "password": vm.data.password
        })
        .then(function(response){
          $log.debug("Received student login: ", response);
          Materialize.toast("Successfully logged in", 2000);

          $cookies.put('access_token', response.id);
          $cookies.put('userId', response.userId);
          $cookies.put('type', "student");

          if(response.initForm == 0)
            $state.go('studentEmailconf');
          else if(response.initForm == 1)
            $state.go('studentInitForm', {id: response.userId});
          else $state.go('studentProfile', { id: response.userId });
        })
        .catch(function(response){
          $log.warn("Failed student login: ", response);
          Materialize.toast("Failed to login", 2000);
        });
    };

  }

})();
