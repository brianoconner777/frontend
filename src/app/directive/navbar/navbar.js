(function() {
  'use strict';

  angular
    .module('frontend')
    .directive('navbar', navbar);

  function navbar(){

    var directive = {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/directive/navbar/navbar.html',
      link: link,
      controller: 'navbarController'
    };
    return directive;

    function link(scope, element, attrs){

    }

  }

})();
