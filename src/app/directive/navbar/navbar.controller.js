'use strict';

angular
  .module('frontend')
  .controller('navbarController', navbarController);

function navbarController($scope, $rootScope, $cookies, $state, Student, Teacher) {
  var vm = $scope;
  vm._ = _;
  vm.name = "";
  vm.img = "";
  vm.disable = false;

  $scope.toggleProfile = function() {
    $rootScope.$broadcast('updateProfileToggle', $scope.toggleProfile);
  };

  vm.myProfile = function() {
    if ($cookies.get("type") == "student")
      $state.go("studentProfile", {id: $cookies.get("userId")});
    else $state.go("teacherProfile", {id: $cookies.get("userId")});
  }

  if ($cookies.get("type") == "student") {
    Student.getStudentById($cookies.get('userId'), $cookies.get('access_token'))
      .then(function(data) {
        vm.name = data.name;
        vm.img = data.image;
      })
  } else if ($cookies.get("type") == "teacher") {
    Teacher.getTeacherById($cookies.get('userId'), $cookies.get('access_token'))
      .then(function(data) {
        vm.name = data.name;
        vm.img = data.image;
      })
  } else {
    vm.disable = true;
    vm.name = "Anonymous";
    vm.img = "";
  }



}
