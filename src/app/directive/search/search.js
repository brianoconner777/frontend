(function() {
  'use strict';

  angular
    .module('frontend')
    .directive('search', search);

  function search(){

    var directive = {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/directive/search/search.html',
      link: link,
      controller: 'searchController'
    };
    return directive;

    function link(scope, element, attrs){

    }

  }

})();
