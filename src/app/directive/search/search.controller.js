'use strict';

angular
  .module('frontend')
  .controller('searchController', searchController);

function searchController($scope, $cookies, $log, School, Student) {
  var vm = $scope;
  vm._ = _;

  vm.context = {};

  vm.searchDisable = true;
  vm.openSearch = function(){
    $scope.searchDisable = false;
    $scope.searchFocus = true;
    $scope.searchTerm = "";
  };

  vm.closeSearch = function(){
    $scope.searchDisable = true;
  };

  vm.search = function () {
    var filters = {
      "filter[where][schoolId]": $cookies.get("schoolId"),
      "filter[where][initForm][gt]": 1,
      "filter[where][name][regexp]": "/"+vm.context.searchTerm+"/i",

      "filter[fields][username]": true,
      "filter[fields][name]": true,
      "filter[fields][image]": true,

      "filter[limit]": 14
    };

    Student.find(filters, $cookies.get('access_token'))
      .then(function(d){
        vm.results = d;
        $log.debug('GET Find Student:', d);
      })
      .catch(function(err) {
        $log.error('Cannot GET Find Student:', err);
      });
  }


}

// vm.results = [
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"},
// {name: "David Gandy"}
// ];
